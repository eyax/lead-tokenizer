#include <iostream>
#include "Tokenizer.hpp"
#include <fstream>

int main(int argc, char** argv)
{
    if(argc == 1)
    {
        std::cout << "Error : No input file" << std::endl;
        exit(-1);
    }

    std::fstream file(argv[1]); //First argument is the file
    std::string fileContent;

    std::string temp;

    while(std::getline(file, temp))
    {
        fileContent += temp + "\n";
    }

    file.clear();
    temp.clear();

    lead::Tokenizer tokenizer;
    for(const auto& token : tokenizer.tokenize(fileContent))
    {
        std::cout << "{'" << token.literal << "', " << token.type << "}" << std::endl;
    }

    return 0;
}