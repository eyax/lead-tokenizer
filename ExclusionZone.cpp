/*
 * @author underdisk
 */

#include "ExclusionZone.hpp"

namespace lead
{
    bool ExclusionZoneContext::canBeToken(const std::string &literal, unsigned int position) const
    {
        for(const auto& excZone : m_zones)
        {
            if(position > excZone.start && position < excZone.end) //
            {
                return false;
            }
        }
        return true;
    }

    void ExclusionZoneContext::addExclusionZone(int start, int end)
    {
        m_zones.emplace_back(ExclusionZone(start, end));
    }
}