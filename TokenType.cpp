//
// Created by Hébriel ROUSSEAU on 2019-06-18.
//

#include "TokenType.hpp"

namespace lead
{
    TokenType::TokenType(const std::string& regex, const std::string& name, ExclusionZoneContext& context)
        : m_context(context)
    {
        setRegex(regex);
        m_name = name;
    }

    const std::regex& TokenType::getRegex() const
    {
        return  *m_regex;
    }

    const std::string& TokenType::getName() const
    {
        return m_name;
    }

    ExclusionZoneContext& TokenType::getContext() const
    {
        return m_context;
    }

    void TokenType::setRegex(const std::string& regex)
    {
        m_regex = std::make_unique<std::regex>(regex);
    }

    void TokenType::createToken(const std::string& file, std::vector<lead::Token>& tokens)
    {
        auto regex_begin = std::sregex_iterator(file.begin(), file.end(), getRegex());
        auto regex_end = std::sregex_iterator();

        for(std::sregex_iterator i = regex_begin; i != regex_end; ++i)
        {
            Token token(i->position(), '\'' + i->str() + '\'', getName());
            if(getContext().canBeToken(token.type, token.position))
                tokens.emplace_back(token);
        }
    }

    ExclusiveTokenType::ExclusiveTokenType(const std::string& regex, const std::string& name, lead::ExclusionZoneContext& context)
        : TokenType(regex, name, context)
    {   }

    void ExclusiveTokenType::createToken(const std::string& file, std::vector<lead::Token>& tokens)
    {
        auto regex_begin = std::sregex_iterator(file.begin(), file.end(), getRegex());
        auto regex_end = std::sregex_iterator();

        for(std::sregex_iterator i = regex_begin; i != regex_end; ++i)
        {
            Token token(i->position(), i->str(), getName());
            if(getContext().canBeToken(token.literal + " of type " + token.type, token.position))
            {
                tokens.emplace_back(token);
                getContext().addExclusionZone(token.position, token.position + i->str().size());
            }
        }
    }

	const std::string &ExclusiveTokenType::getName() const
	{
		return TokenType::getName();
	}
}